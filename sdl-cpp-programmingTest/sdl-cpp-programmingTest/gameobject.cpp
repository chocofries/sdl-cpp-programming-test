#include "gameobject.h"


GameObject::~GameObject()
{
}

void GameObject::Start()
{
}

void GameObject::Update()
{
}

void GameObject::Render()
{
}

bool GameObject::GetIsActive()
{
	return isActive;
}

void GameObject::SetIsActive(bool _isActive)
{
	isActive = _isActive;
}

float GameObject::GetX()
{
	return x;
}

float GameObject::GetY()
{
	return y;
}

void GameObject::SetX(float _x)
{
	x = _x;
}

void GameObject::SetY(float _y)
{
	y = _y;
}

float GameObject::GetScale()
{
	return scale;
}

void GameObject::SetScale(float _scale)
{
	scale = _scale;
}

void GameObject::RenderCircle(float _radius, float _roundness, float _x, float _y, bool _outline)
{
    float diameter = (_radius * 2);

    float xx = (_radius - 1);
    float yy = 0;
    float tx = _roundness;
    float ty = _roundness;
    float fault = (tx - diameter);

    while (xx >= yy)
    {
        if (!_outline)
        {
            SDL_RenderDrawLineF(renderer, _x + yy, _y - xx, _x + yy, _y + xx);
            SDL_RenderDrawLineF(renderer, _x - yy, _y - xx, _x - yy, _y + xx);
            SDL_RenderDrawLineF(renderer, _x + xx, _y - yy, _x + xx, _y + yy);
            SDL_RenderDrawLineF(renderer, _x - xx, _y - yy, _x - xx, _y + yy);
        }
        else
        {
            SDL_RenderDrawPointF(renderer, _x + xx, _y - yy);
            SDL_RenderDrawPointF(renderer, _x + xx, _y + yy);
            SDL_RenderDrawPointF(renderer, _x - xx, _y - yy);
            SDL_RenderDrawPointF(renderer, _x - xx, _y + yy);
            SDL_RenderDrawPointF(renderer, _x + yy, _y - xx);
            SDL_RenderDrawPointF(renderer, _x + yy, _y + xx);
            SDL_RenderDrawPointF(renderer, _x - yy, _y - xx);
            SDL_RenderDrawPointF(renderer, _x - yy, _y + xx);
        }

        if (fault <= 0)
        {
            ++yy;
            fault += ty;
            ty += 2;
        }

        if (fault > 0)
        {
            --xx;
            tx += 2;
            fault += (tx - diameter);
        }
    }
}
