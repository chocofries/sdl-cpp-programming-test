#include <SDL.h>
#include "game.h"
#include "scenegame.h"
#include "ball.h"
#include "paddle.h"
#include "brick.h"

SceneGame::SceneGame(int _id)
{
	id = _id;
	gameObjects = new vector<GameObject*>();
	bricks = new vector<Brick*>();
	ball = nullptr;

	Start();
}

SceneGame::~SceneGame()
{
	Scene::~Scene();
	delete bricks;
	delete gameObjects;
}

void SceneGame::Start()
{
	// Create game objects, add to vector of game objects

	// Ball
	ball = new Ball(gameObjects->size(), this);
	gameObjects->push_back(ball);

	// Paddle
	paddle = new Paddle(gameObjects->size(), this, ball);
	gameObjects->push_back(paddle);

	// Bricks
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			Brick* brick = new Brick(gameObjects->size(), this, ball, 60.0f + (i*70.0f), 20.0f + (j*28.0f));
			gameObjects->push_back(brick);
			bricks->push_back(brick);
		}
	}
}

void SceneGame::Update()
{
	deltaTime = Game::GetGameInstance().GetDeltaTime();
	Scene::Update();
}

void SceneGame::Render()
{
	// Draw blue background
	SDL_Rect bgRect;
	bgRect.x = 0;
	bgRect.y = 0;
	bgRect.w = 750;
	bgRect.h = 500;

	SDL_SetRenderDrawColor(Game::GetGameInstance().GetRenderer(), 37, 40, 61, 255); 
	SDL_RenderFillRect(Game::GetGameInstance().GetRenderer(), &bgRect);

	Scene::Render();
}

void SceneGame::Reset()
{
	// Re-activate all bricks
	for (auto it = bricks->begin(); it != bricks->end(); it++)
	{
		Brick* b = *it;
		b->SetIsActive(true);
		b->ResetBrick();
	}
}
