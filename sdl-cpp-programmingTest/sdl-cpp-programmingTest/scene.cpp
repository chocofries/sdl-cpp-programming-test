#include "scene.h"
#include "gameobject.h"

Scene::~Scene()
{
	while (!gameObjects->empty())
	{
		// Delete all game objects from memory
		GameObject* temp = gameObjects->back();
		gameObjects->pop_back();
		delete temp;
	}
}

void Scene::Start()
{
}

void Scene::Update()
{
	// Iterate through all game objects and update them if they are active
	for (auto it = gameObjects->begin(); it != gameObjects->end(); it++)
	{
		GameObject* g = *it;
		if (g->GetIsActive())
		{
			g->Update();
		}
	}
}

void Scene::Render()
{
	// Iterate through all game objects and render them if they are active
	for (auto it = gameObjects->begin(); it != gameObjects->end(); it++)
	{
		GameObject* g = *it;
		if (g->GetIsActive())
		{
			g->Render();
		}
	}
}

void Scene::Reset()
{
}

float Scene::GetDeltaTime() const
{
	return deltaTime;
}
