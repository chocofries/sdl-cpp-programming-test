#pragma once
#include "scene.h"

class Ball;
class Paddle;
class Brick;

class SceneGame : public Scene
{
public:
	SceneGame(int _id);
	virtual ~SceneGame();

	virtual void Start();
	virtual void Update();
	virtual void Render();
	virtual void Reset();

private:
	Ball* ball;
	Paddle* paddle;
	vector<Brick*>* bricks;
};