#include "game.h"
#include <ctime>


int main( int argc, char* args[] )
{
	// Random Seed
	srand((unsigned) time(0));

	Game* game = &Game::GetGameInstance();
	game->Initialise();

	Uint64 now = SDL_GetPerformanceCounter();
	Uint64 last = 0;
	double deltaTime = 0;

	SDL_ShowCursor(false);

	while (game->GetIsGameRunning() == true)
	{
		// Calculate delta time
		last = now;
		now = SDL_GetPerformanceCounter();
		deltaTime = (double)((now - last) * 1000 / (double)SDL_GetPerformanceFrequency());

		game->SetDeltaTime((float)deltaTime);

		game->Event();
		game->Update();
		game->Render();
	}

	game->DestroyGameInstance();

	return 0;
}