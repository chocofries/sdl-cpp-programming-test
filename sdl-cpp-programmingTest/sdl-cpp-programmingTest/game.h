#pragma once
#include <SDL.h>
#include <stdio.h>
#include "scene.h"

using namespace std;

class Game
{
public:
	Game();
	~Game();

	void Initialise();
	void Start();
	void Event();
	void Update();
	void Render();

	bool GetIsGameRunning();
	static Game& GetGameInstance();
	static void DestroyGameInstance();

	float GetDeltaTime() const;
	void SetDeltaTime(float _dt);

	SDL_Renderer* GetRenderer();

protected:
	static Game* gameInstance;

	SDL_Window* window;
	SDL_Renderer* renderer;
	Scene* currentScene;

private:
	vector<Scene*>* scenes;
	bool gameIsRunning;
	float deltaTime;
};