#include "brick.h"
#include "ball.h"
#include "game.h"
#include "utils.h"

Brick::Brick(int _id, Scene* _scene, Ball* _ball, float _x, float _y)
{
	id = _id;
	scene = _scene;
	ball = _ball;
	x = _x;
	y = _y;
	scale = 1.0f;
	renderer = Game::GetGameInstance().GetRenderer();
	isActive = true;
	deathDelay = 1.0f;
	isHit = false;

	Start();
}

Brick::~Brick()
{
}

void Brick::Start()
{
}

void Brick::Update()
{
	// Reduce alpha when hit
	if (isHit)
	{
		if (deathDelay <= 0.0f)
		{
			isActive = false;
		}
		else
		{
			deathDelay -= 0.1f * scene->GetDeltaTime();
		}
	}

	// Scaling effect
	scale = Utils::Clamp(scale, 1.0f, 1.2f);
	if (scale > 1.0f)
	{
		scale -= 0.01f * scene->GetDeltaTime();
	}

	// Check for collision with ball, then reflect
	float bx = ball->GetX();
	float by = ball->GetY();

	if (!isHit)
	{
		if (bx >= x - 30.0f && bx <= x + 30.0f)
		{
			if (by <= y + 10.0f && by >= y - 10.0f)
			{
				// Find the reflection direction
				Direction dir = Direction::Down;
				float angle = ball->GetAngle();

				if (angle > 45.0f && angle <= 135.0f)
				{
					dir = Direction::Down;
				}
				else if (angle > 225.0f && angle <= 315.0f)
				{
					dir = Direction::Up;
				}

				ball->Reflect(dir);
				ball->AddBonusSpeed(0.01f);
				ball->AddAngle(10.0f - ((rand() % 200) * 0.1f));
				scale += 0.5f;
				isHit = true;
			}
		}
	}
}

void Brick::Render()
{
	// Draw brick
	SDL_Rect rect;
	rect.x = (int)(x - (30.0f * scale));
	rect.y = (int)(y - (10.0f * scale));
	rect.w = (int)(60 * scale);
	rect.h = (int)(20 * scale);

	SDL_SetRenderDrawColor(Game::GetGameInstance().GetRenderer(), 58, 69, 150, 255);
	SDL_RenderFillRect(Game::GetGameInstance().GetRenderer(), &rect);
}

void Brick::ResetBrick()
{
	isHit = false;
	scale = 1.0f;
	deathDelay = 1.0f;
}
