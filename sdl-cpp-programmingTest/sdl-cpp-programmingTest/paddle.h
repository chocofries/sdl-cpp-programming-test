#pragma once
#include "gameobject.h"

class Ball;

class Paddle : public GameObject
{
public:
	Paddle(int _id, Scene* _scene, Ball* _ball);
	virtual ~Paddle();

	virtual void Start();
	virtual void Update();
	virtual void Render();

private:
	int mx;
	int my;
	Ball* ball;
	float speed;
};