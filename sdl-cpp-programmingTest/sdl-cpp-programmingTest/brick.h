#pragma once
#include "gameobject.h"

class Ball;

class Brick : public GameObject
{
public:
	Brick(int _id, Scene* _scene, Ball* _ball, float _x, float _y);
	virtual ~Brick();

	virtual void Start();
	virtual void Update();
	virtual void Render();

	void ResetBrick();

private:
	Ball* ball;
	float deathDelay;
	bool isHit;
};