#pragma once
#include <iostream>
#include <vector>

using namespace std;

class GameObject;
class Game;

class Scene
{
public:
	virtual ~Scene();

	virtual void Start();
	virtual void Update();
	virtual void Render();
	virtual void Reset();

	float GetDeltaTime() const;

protected:
	int id;
	float deltaTime;
	vector<GameObject*>* gameObjects;
};