#include "paddle.h"
#include "ball.h"
#include "game.h"
#include "utils.h"

Paddle::Paddle(int _id, Scene* _scene, Ball* _ball)
{
	id = _id;
	scene = _scene;
	ball = _ball;

	x = 325.0f;
	y = 460.0f;
	mx = 0;
	my = 0;
	scale = 1.0f;
	renderer = Game::GetGameInstance().GetRenderer();
	isActive = true;

	Start();
}

Paddle::~Paddle()
{
}

void Paddle::Start()
{
}

void Paddle::Update()
{
	// Scaling effect
	scale = Utils::Clamp(scale, 1.0f, 1.5f);
	if (scale > 1.0f)
	{
		scale -= 0.01f * scene->GetDeltaTime();
	}

	// Get paddle to follow mouse
	SDL_GetMouseState(&mx, &my);
	x = (float)mx;

	// Check for collision with ball, then reflect
	float bx = ball->GetX();
	float by = ball->GetY();

	if (bx >= x - 40.0f && bx <= x + 40.0f)
	{
		if (by >= y - 15.0f)
		{
			ball->Reflect(Direction::Up);
			ball->AddAngle(ball->GetX() - x);
			scale += 0.5f;
		}
	}
}

void Paddle::Render()
{
	// Draw mini cursor
	RenderCircle(5.0f, 2.0f, (float)mx, (float)my, true);

	// Draw paddle
	SDL_Rect rect;
	rect.x = (int)(x - (30.0f * scale));
	rect.y = (int)(y - (10.0f * scale));
	rect.w = (int)(60 * scale);
	rect.h = (int)(20 * scale);

	SDL_SetRenderDrawColor(Game::GetGameInstance().GetRenderer(), 58, 69, 150, 255);
	SDL_RenderFillRect(Game::GetGameInstance().GetRenderer(), &rect);
}
