#include "game.h"
#include "scenegame.h"

const int SCREEN_WIDTH = 750;
const int SCREEN_HEIGHT = 500;

// Game instance
Game* Game::gameInstance = 0;

Game::Game()
{
}

Game::~Game()
{
	while (!scenes->empty())
	{
		// Delete all scenes from memory
		Scene* temp = scenes->back();
		scenes->pop_back();
		delete temp;
	}

	if (scenes != nullptr)
	{
		delete scenes;
	}
}

void Game::Initialise()
{
	gameIsRunning = false;
	window = nullptr;
	renderer = nullptr;
	scenes = new vector<Scene*>();
	currentScene = nullptr;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("Game could not initialize SDL! SDL_Error: %s\n", SDL_GetError());
		gameIsRunning = false;
	}
	else
	{
		// Create window
		window = SDL_CreateWindow("SDL C++ Programming Test - John Bu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
		else
		{
			cout << "Window created." << endl;
		}

		// Create renderer
		renderer = SDL_CreateRenderer(window, -1, 0);
		if (renderer == NULL)
		{
			printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
		}
		else
		{
			cout << "Renderer created." << endl;
			// Set default render colour to purple
			SDL_SetRenderDrawColor(renderer, 205, 0, 250, 255);
		}

		gameIsRunning = true;
		Start();
	}
}

void Game::Start()
{
	// Create scene
	Scene* s = new SceneGame(scenes->size());
	scenes->push_back(s);

	// Set as current scene
	currentScene = s;
}

void Game::Event()
{
	SDL_Event event;
	SDL_PollEvent(&event);

	// Check the event
	switch (event.type)
	{
	case SDL_QUIT:
		gameIsRunning = false;
		break;
	default:
		break;
	}
}

void Game::Update()
{
	// Update the current scene
	if (currentScene != nullptr)
	{
		currentScene->Update();
	}
}

void Game::Render()
{
	// Set default render colour to purple
	SDL_SetRenderDrawColor(renderer, 205, 0, 250, 255);
	SDL_RenderClear(renderer);

	// Render the current scene
	if (currentScene != nullptr)
	{
		currentScene->Render();
	}

	SDL_RenderPresent(renderer);
}

bool Game::GetIsGameRunning()
{
	return gameIsRunning;
}

Game& Game::GetGameInstance()
{
	if (gameInstance == 0)
	{
		gameInstance = new Game();
	}

	return (*gameInstance);
}

void Game::DestroyGameInstance()
{
	SDL_DestroyWindow(GetGameInstance().window);
	SDL_DestroyRenderer(GetGameInstance().renderer);
	SDL_Quit();

	cout << "Game instance destroyed..." << endl;

	delete gameInstance;
	gameInstance = NULL;
}

float Game::GetDeltaTime() const
{
	return deltaTime;
}

void Game::SetDeltaTime(float _dt)
{
	deltaTime = _dt;
}

SDL_Renderer* Game::GetRenderer()
{
	return renderer;
}
