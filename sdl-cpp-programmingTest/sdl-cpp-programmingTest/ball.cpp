#include "ball.h"
#include "game.h"
#include "utils.h"

Ball::Ball(int _id, Scene* _scene)
{
	id = _id;
    scene = _scene;

	x = 325.0f;
	y = 250.0f;
	scale = 1.0f;
	renderer = Game::GetGameInstance().GetRenderer();
    isActive = true;
    radius = 10.0f;
    speed = 0.3f;
    bonusSpeed = 0.0f;
    bumpSpeed = 0.0f;
    angle = (float)(rand() % 120) + 30.0f;

	Start();
}

Ball::~Ball()
{
}

void Ball::Start()
{
}

void Ball::Update()
{
    x += Utils::LengthDirX((speed + bonusSpeed + bumpSpeed) * scene->GetDeltaTime(), angle);
    y += Utils::LengthDirY((speed + bonusSpeed + bumpSpeed) * scene->GetDeltaTime(), angle);

    // Hits the walls of the level
    if (x - radius <= 0.0f)
    {
        Reflect(Direction::Right);
    }
    if (y - radius <= 0.0f)
    {
        Reflect(Direction::Down);
    }
    if (x + radius >= 750.0f)
    {
        Reflect(Direction::Left);
    }

    // Hits the bottom of the level
    if (y + radius >= 500.0f)
    {
        x = 325.0f;
        y = 250.0f;
        bonusSpeed = 0.0f;
        bumpSpeed = 0.0f;
        angle = (float)(rand() % 120) + 30.0f;

        scene->Reset();
    }

    // Scale Effect
    if (scale > 1.0f)
    {
        scale = Utils::Clamp(scale, 1.0f, 1.5f);
        scale -= 0.02f * scene->GetDeltaTime();
    }

    // Bump speed
    if (bumpSpeed > 0.0f)
    {
        bumpSpeed -= 0.0025f * scene->GetDeltaTime();
    }
}

void Ball::Render()
{
	// Draw a circle
    SDL_SetRenderDrawColor(Game::GetGameInstance().GetRenderer(), 58, 69, 150, 255);

    if (bumpSpeed > 0.4f)
    {
        RenderCircle(radius * scale, 2, x, y, true);
    }
    else
    {
        RenderCircle(radius * scale, 2, x, y, false);
    }
}

float Ball::GetBonusSpeed() const
{
    return bonusSpeed;
}

float Ball::GetAngle() const
{
    return angle;
}

void Ball::AddBonusSpeed(float _bonus)
{
    bonusSpeed += _bonus;
}

void Ball::AddAngle(float _degrees)
{
    angle += _degrees;
}

void Ball::Reflect(Direction _normalDir)
{
    // Normal of wall
    float nx = 0.0f;
    float ny = 0.0f;
    float nAngle = 0.0f;

    switch (_normalDir)
    {
    case Direction::Up:
        nx = 0.0f;
        ny = 1.0f;
        nAngle = 270.0f;
        break;
    case Direction::Down:
        nx = 0.0f;
        ny = -1.0f;
        nAngle = 90.0f;
        break;
    case Direction::Left:
        nx = 1.0f;
        ny = 0.0f;
        nAngle = 0.0f;
        break;
    case Direction::Right:
        nx = -1.0f;
        ny = 0.0f;
        nAngle = 180.0f;
        break;
    default:
        break;
    }

    // Reflect the ball's angle
    float ix = Utils::LengthDirX(1.0f, angle);
    float iy = Utils::LengthDirY(1.0f, angle);
    float dot = (ix * nx + iy * ny);
    float rx = ix - (2.0f * dot * nx);
    float ry = iy - (2.0f * dot * ny);
    angle = (atan2(ry, rx) / MATH_PI) * 180.0f;

    // Ball scale
    scale += 0.75f;
    // Add bump speed
    bumpSpeed = 0.5f;
}
