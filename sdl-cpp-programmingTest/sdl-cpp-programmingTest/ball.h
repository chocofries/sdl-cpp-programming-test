#pragma once
#include "gameobject.h"

class Ball : public GameObject
{
public:
	Ball(int _id, Scene* _scene);
	virtual ~Ball();

	virtual void Start();
	virtual void Update();
	virtual void Render();

	float GetBonusSpeed() const;
	float GetAngle() const;
	void AddBonusSpeed(float _bonus);
	void AddAngle(float _degrees);
	void Reflect(Direction _dir); // Reflects the ball in a cardinal direction

private:
	float speed;
	float bumpSpeed;
	float bonusSpeed;
	float radius;
	float angle;
};