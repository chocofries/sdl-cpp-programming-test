#pragma once
#include <SDL.h>
#include "scene.h"

class GameObject
{
public:
	enum class Direction
	{
		Up,
		Down,
		Left,
		Right
	};

	virtual ~GameObject();

	virtual void Start();
	virtual void Update();
	virtual void Render();

	bool GetIsActive();
	void SetIsActive(bool _isActive);

	float GetX();
	float GetY();
	void SetX(float _x);
	void SetY(float _y);

	float GetScale();
	void SetScale(float _scale);

	void RenderCircle(float _radius, float _roundness, float _x, float _y, bool _outline);

protected:
	Scene* scene;
	SDL_Renderer* renderer;
	bool isActive;

	int id;
	float x;
	float y;
	float scale;
};