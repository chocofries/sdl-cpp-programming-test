# README #

## Instructions ##

### Breakout ###
The goal of the game is to break all of the bricks by bouncing the ball into them.
If the ball falls through the bottom of the level without being hit by the paddle, then it is game over.
The game automatically restarts when it is game over.

### Controls ###
Use your mouse to control the position of the paddle.